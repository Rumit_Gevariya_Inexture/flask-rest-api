# .env file constants : The saved ones should be in capital letters like SECRET_KEY
EMAIL_REGEX = r"^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w+$"
PASSWORD_REGEX = r"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!#%*?&]{8,12}$"
USER_NAME_REGEX = r"^[A-Za-z\d@$!_#%*?&]{3,30}$"

ENV_FLASK_SERVER_TYPE = "FLASK_ENV"  # This is for choosing a server type
ENV_SECRET_KEY = "SECRET_KEY"
ENV_JWT_SECRET_KEY = "JWT_SECRET_KEY"
ENV_DEVELOPMENT_DB_URL = "DEVELOPMENT_DB_URL"
ERR_SET_DATABASE_VARIABLES = ""

ERR_SQL_ALCHEMY_ERROR = "Error in storing database."
MSG_REGISTER_USER_SUCCESSFULLY = "Hey {} you registered successfully."
ERR_USER_NAME_ALREADY_TAKEN = "The user name {} is already taken."
ERR_EMAIL_ALREADY_TAKEN = "The Email {} is already taken."
ERR_PASSWORD_INCORRECT = "The password is incorrect"
ERR_USER_WITH_USER_NAME_NOT_EXISTS = "There is no user with user_name {}"
MSG_LOG_IN_SUCCESSFULLY = "Successfully Logged in."
MSG_FOR_ACCESS_TOKEN_REFRESH = "Your access token is successfully refresh."
MSG_LOG_OUT_SUCCESSFULLY = "Successfully Logged out."


MSG_REGISTER_BLOG_SUCCESSFULLY = "Hey {} your blog post created successfully."
ERR_BLOG_NOT_EXISTS = "User with blog_id {} does not exists."
MSG_RETRIEVE_BLOG = "Successfully retrieved blog."
MSG_RETRIEVE_BLOGS = "Successfully retrieved blogs."
ERR_NO_DATA_IN_BLOG = "There is no blog available in database."
MSG_DELETED_BLOG = "Blog deleted successfully."
ERR_BLOG_ID_NOT_PROVIDE = "Blog id not provide."
MSG_UPDATE_BLOG = "Successfully updated blog."
