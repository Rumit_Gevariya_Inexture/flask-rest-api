"""
    Class-based Flask app configuration.
    config class is for base configuration.
    you can change value in inheritance class of config and create your as per need
    https://flask.palletsprojects.com/en/1.1.x/config/#environment-and-debug-features
    https://hackersandslackers.com/configure-flask-applications/
    https://flask.palletsprojects.com/en/1.1.x/api/#configuration
"""
import os
import datetime
from my_project.constant import ENV_FLASK_SERVER_TYPE, ENV_DEVELOPMENT_DB_URL, ENV_SECRET_KEY, ENV_JWT_SECRET_KEY
from dotenv import load_dotenv, find_dotenv


# This will load the .env file from the root directory and export the environment variables to the environment.
# basedir = os.path.abspath(os.path.dirname(__file__))


SERVER_TYPE_PRODUCTION = "production"
SERVER_TYPE_DEVELOPMENT = "development"

# database variables
ERR_SET_DATABASE_VARIABLES = "Set database configuration key in environment"
# JWT variable
ERR_SET_JWT_VARIABLE = "Set JWT_SECRET_KEY in environment"


def get_current_server_config():
    """
    This will check FLASK_ENV variable and create an object of configuration according to that
    :return: Production or Development Config object
    """
    current_config = os.environ.get(ENV_FLASK_SERVER_TYPE, SERVER_TYPE_DEVELOPMENT)
    return DevelopmentConfig() if current_config == SERVER_TYPE_DEVELOPMENT else ProductionConfig()


class Config(object):
    """Set base configuration, env variable configuration and service configuration"""

    # The starting execution point of the app
    FLASK_APP = 'app.py'
    JSON_SORT_KEYS = False  # For json response, by default jsonify sorts the response keys alphabetically


class ProductionConfig(Config):
    """ This class for generates the config for production instance"""
    DEBUG = False
    TESTING = False


class DevelopmentConfig(Config):
    """ This class for generates the config for development instance"""
    DEBUG = True
    TESTING = True

    # in production we will be use path to this file in actual server path
    load_dotenv(find_dotenv(".env"))
    # database configuration
    SQLALCHEMY_ECHO = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    if os.environ.get('ENV_DEVELOPMENT_DB_URL'):
        SQLALCHEMY_DATABASE_URI = os.getenv("ENV_DEVELOPMENT_DB_URL")
    else:
        # Todo: should be logging
        print(ERR_SET_DATABASE_VARIABLES)
    if os.environ.get('ENV_SECRET_KEY'):
        SECRET_KEY = os.getenv("ENV_SECRET_KEY")


    # JWT Token configuration
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(hours=48)
    JWT_REFRESH_TOKEN_EXPIRES = False
    if os.environ.get('ENV_JWT_SECRET_KEY'):
        JWT_SECRET_KEY = os.getenv("ENV_JWT_SECRET_KEY")
    else:
        print(ERR_SET_JWT_VARIABLE)


