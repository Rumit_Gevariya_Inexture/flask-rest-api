from my_project import marshmallow
from flask_marshmallow.fields import fields
from my_project.blog.models import Blog
from my_project.user.schema import UserRegisterResponseSchema


class BlogRequestSchema(marshmallow.SQLAlchemyAutoSchema):
    class Meta:
        model = Blog
        exclude = ("date_posted", "owner_id")


class BlogResponseSchema(marshmallow.SQLAlchemyAutoSchema):
    owner = fields.Nested(UserRegisterResponseSchema())

    class Meta:
        model = Blog
        exclude = ("owner_id",)
        include_relationships = True


class BlogUpdateSchema(marshmallow.Schema):
    title = fields.Str()
    content = fields.Str()