from flask_restful import Api, Resource
from flask import Blueprint, request
from flask_accept import accept
from flask_jwt_extended import jwt_required
from my_project.blog import request_response_handler


# Blueprint Configuration
blog_bp = Blueprint('blog_bp', __name__)
blog_rest_api = Api(blog_bp)


# Router Inside a Blueprint

class BlogResource(Resource):

    @jwt_required()
    def get(self, blog_id=None):
        """
        This is called when request method is get.
        :param blog_id: id of user
        :return:
        """
        return request_response_handler.create_response_of_blog(
            blog_id) if blog_id else request_response_handler.create_response_of_blogs()

    @accept("application/json")
    @jwt_required()
    def post(self):
        """
        This is call when request method is post.
        :return:
        """
        req_data = request.get_json()
        return request_response_handler.create_response_of_create_blog(req_data)

    @accept("application/json")
    @jwt_required()
    def delete(self, blog_id=None):
        """
        This is call when request method is delete.
        :param: blog_id
        :return:
        """
        return request_response_handler.create_response_of_delete_blog(blog_id)

    @accept("application/json")
    @jwt_required()
    def put(self, blog_id=None):
        """
        This is call when request method is update.
        :return:
        """
        req_data = request.get_json()
        return request_response_handler.create_response_of_update_blog(req_data, blog_id)


blog_rest_api.add_resource(BlogResource, '/api/v1/blog/', '/api/v1/blog/<int:blog_id>',)
