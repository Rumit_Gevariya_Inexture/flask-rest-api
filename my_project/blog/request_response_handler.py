# create responses of blog module's api here
"""
This file checks the request is proper or not.
and this must also create the Response object, all other business logic can be there in the other file.
"""
from http import HTTPStatus
from my_project.request_utils import validate_req_schema
from my_project.blog.schema import BlogRequestSchema, BlogUpdateSchema
from my_project.blog import services
from my_project import response_utils


@validate_req_schema(BlogRequestSchema())
def create_response_of_create_blog(request_data):
    """
    From this request get the required params and if all available then proceed
    otherwise send missing params response
    :param request_data: Data from API request
    :return: Json formed response
    """
    http_status_code, message, data = services.create_blog(**request_data)
    if http_status_code == HTTPStatus.CREATED:
        return response_utils.send_success_response(message=message, data=data, status_code=http_status_code)

    return response_utils.send_error_response(status_code=http_status_code, message=message)


def create_response_of_blog(blog_id):
    """
       This method is used to fetch particular user
       :param blog_id: id of blog
       :return:Json formed response
       """
    # Note: In case you need to check on multiple status code
    http_status_code, message, data = services.fetch_blog_by_id(blog_id)
    if http_status_code == HTTPStatus.OK:
        return response_utils.send_success_response(message=message, data=data, status_code=http_status_code)

    return response_utils.send_error_response(status_code=http_status_code, message=message)


def create_response_of_blogs():
    """
        This method is used to fetch all user blogs.
        :return:Json formed response
     """
    http_status_code, message, data = services.fetch_all_user_blogs()
    if http_status_code == HTTPStatus.OK:
        return response_utils.send_success_response(message=message, data=data, status_code=http_status_code)

    return response_utils.send_error_response(status_code=http_status_code, message=message)


def create_response_of_delete_blog(blog_id):
    """
        This api is used to delete blog.
        :param: blog_id
        :return:Json formed response
        """
    http_status_code, message = services.delete_blog(blog_id)
    if http_status_code == HTTPStatus.NO_CONTENT:
        return response_utils.send_success_response(message=message, status_code=http_status_code)

    return response_utils.send_error_response(status_code=http_status_code, message=message)


@validate_req_schema(BlogUpdateSchema())
def create_response_of_update_blog(request_data, blog_id):
    """
            This api is used to update blog.
            :param: request data
            :param: blog_id
            :return:Json formed response
            """
    http_status_code, message = services.update_blog(request_data, blog_id)
    if http_status_code == HTTPStatus.NO_CONTENT:
        return response_utils.send_success_response(message=message, status_code=http_status_code)

    return response_utils.send_error_response(status_code=http_status_code, message=message)

