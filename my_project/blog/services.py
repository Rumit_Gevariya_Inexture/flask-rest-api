from http import HTTPStatus
from flask_jwt_extended import get_jwt_identity
from my_project.user.models import User
from sqlalchemy.exc import SQLAlchemyError
from flask import current_app as app
from my_project import constant,database_conn
from my_project.blog.models import Blog
from my_project.blog.schema import BlogResponseSchema


def create_blog(**kwargs):
    """
       This method takes all required parameters and register user.
       :param kwargs: dict of blog data.
       :return: tuple of Response components, For success or Error
       """
    try:
        user_id = get_jwt_identity()
        user = User.query.filter(User.user_id == user_id).first()
        user_name = user.user_name

        # append the current use id into payload data.
        kwargs["owner_id"] = user_id
        blog = Blog(kwargs)
        blog.save()
        success_msg = constant.MSG_REGISTER_BLOG_SUCCESSFULLY.format(user_name)
        return HTTPStatus.CREATED, success_msg, None
    except SQLAlchemyError as e:
        app.logger.error(e.__str__())
        return HTTPStatus.INTERNAL_SERVER_ERROR, constant.ERR_SQL_ALCHEMY_ERROR, None


def fetch_blog_by_id(blog_id):
    """
       This method fetch blog by blog_id.
       :param blog_id: id of blog
       :return: tuple of Response components, For success or Error
       """
    if blog := Blog.query.get(blog_id):
        data = BlogResponseSchema().dump(blog)
        return HTTPStatus.OK, constant.MSG_RETRIEVE_BLOG, data

    return HTTPStatus.NOT_FOUND, constant.ERR_BLOG_NOT_EXISTS.format(blog_id), None


def fetch_all_user_blogs():
    """
        This method fetches all current user blogs available in the  database.
        :return: tuple of Response components, For success or Error
        """
    user_id = get_jwt_identity()
    if blogs := Blog.query.filter_by(owner_id=user_id).all():
        data = BlogResponseSchema(many=True).dump(blogs)
        return HTTPStatus.OK, constant.MSG_RETRIEVE_BLOGS, data

    return HTTPStatus.NOT_FOUND, constant.ERR_NO_DATA_IN_BLOG, None


def delete_blog(blog_id):
    """
       This method will delete blog by its id.
       :param: blog_id
       :return: tuple of Response components, For success or Error
       """
    try:
        user_id = get_jwt_identity()
        if not blog_id:
            return HTTPStatus.BAD_REQUEST, constant.ERR_BLOG_ID_NOT_PROVIDE
        if not (blog := Blog.query.filter(Blog.blog_id == blog_id, Blog.owner_id == user_id).first()):
            return HTTPStatus.BAD_REQUEST, constant.ERR_BLOG_NOT_EXISTS.format(blog_id)
        Blog.delete(blog)
        return HTTPStatus.NO_CONTENT, constant.MSG_DELETED_BLOG
    except SQLAlchemyError as e:
        app.logger.error(e.__str__())
        return HTTPStatus.INTERNAL_SERVER_ERROR, constant.ERR_SQL_ALCHEMY_ERROR


def update_blog(request_data, blog_id):
    """
          This method will delete blog by its id.
          :param: blog_id
          :param: request_data
          :return: tuple of Response components, For success or Error
          """
    try:
        user_id = get_jwt_identity()
        if not blog_id:
            return HTTPStatus.BAD_REQUEST, constant.ERR_BLOG_ID_NOT_PROVIDE
        if not (blog := Blog.query.filter(Blog.blog_id == blog_id, Blog.owner_id == user_id).first()):
            return HTTPStatus.BAD_REQUEST, constant.ERR_BLOG_NOT_EXISTS.format(blog_id)
        blog.title = request_data.get("title") or blog.title
        blog.content = request_data.get("content") or blog.content
        database_conn.session.commit()
        return HTTPStatus.OK, constant.MSG_UPDATE_BLOG
    except SQLAlchemyError as e:
        app.logger.error(e.__str__())
        return HTTPStatus.INTERNAL_SERVER_ERROR, constant.ERR_SQL_ALCHEMY_ERROR
