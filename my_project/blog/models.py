from datetime import datetime
from my_project import database_conn


class Blog(database_conn.Model):
    # To change actual table name in the database, As by default it will be same as the model name
    __tablename__ = "blogs"

    blog_id = database_conn.Column(database_conn.INTEGER, primary_key=True)
    title = database_conn.Column(database_conn.String(100), nullable=False)
    content = database_conn.Column(database_conn.Text, nullable=False)
    date_posted = database_conn.Column(database_conn.DateTime, nullable=False, default=datetime.utcnow)
    owner_id = database_conn.Column(database_conn.Integer, database_conn.ForeignKey("users.user_id", ondelete="CASCADE"))

    owner = database_conn.relationship("User")

    def __repr__(self):
        return f"< Blog {self.blog_id}>"

    def __init__(self, data):
        self.title = data.get("title")
        self.content = data.get("content")
        self.owner_id = data.get("owner_id")

    def save(self):
        database_conn.session.add(self)
        database_conn.session.commit()

    def delete(self):
        database_conn.session.delete(self)
        database_conn.session.commit()
