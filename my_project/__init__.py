import sys

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_jwt_extended import JWTManager
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from my_project.config import get_current_server_config

database_conn = SQLAlchemy()
bcrypt = Bcrypt()
marshmallow = Marshmallow()
jwt = JWTManager()
migrate = Migrate()

if sys.version_info[0] < 3:
    raise Exception("The system mush be using Python 3 only!")


def create_app():
    """
        Construct the core application.

        Why we are creating Flask app in __init__ is mentioned here:
        https://flask.palletsprojects.com/en/1.1.x/patterns/packages/#simple-packages
        https://hackersandslackers.com/flask-application-factory

        The whole function can be divided in 4 steps below

        >> Create a Flask app object, which derives configuration values (either from a Python class, a config file,
        or environment variables).
        >> Initialize plugins accessible to any part of our app, such as a database (via flask_sqlalchemy),
        Redis (via flask_redis) or user authentication (via Flask-Login).
        >> Import the logic which makes up our app (such as routes).
        >> Register Blueprints.

        """
    # This indicates our own configuration
    app = Flask(__name__, instance_relative_config=False)
    config_obj = get_current_server_config()
    app.config.from_object(config_obj)

    database_conn.init_app(app)  # SQLAlchemy
    bcrypt.init_app(app)  # Password hashing

    marshmallow.init_app(app)  # Flask marshmallow

    jwt.init_app(app)  # Jwt token

    migrate.init_app(app, database_conn)  # database migrations

    # Reference : https://flask.palletsprojects.com/en/1.1.x/api/#flask.Flask.app_context
    with app.app_context():
        # Import and register blueprints here to avoid creating app.py
        from my_project.user.routes import auth_bp
        from my_project.blog.routes import blog_bp
        app.register_blueprint(auth_bp)
        app.register_blueprint(blog_bp)
        # Importing this will register all the handlers automatically in error_handler_utils
        from my_project import error_handler_utils

        return app
