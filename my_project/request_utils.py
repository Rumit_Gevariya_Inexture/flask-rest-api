from functools import wraps
from my_project.response_utils import send_error_response
from http import HTTPStatus
from marshmallow.exceptions import ValidationError


def validate_req_schema(request_schema):
    """
     This function will validate if any parameter is missing in request data.
    :param request_schema
    :return:
    """
    def decorate_func(function):
        """
       The wrapper function for actual logic, as this is decorator
       :param function:
       :return:
       """

        @wraps(function)
        def wrapper(*args, **kwargs):
            """
            :param args: receives data dictionary
            :param kwargs:
            :return:
            """
            request_data = args[0]
            if request_data is None:
                return send_error_response(status_code=HTTPStatus.BAD_REQUEST)
            try:
                data = request_schema.load(request_data)
                if len(args) > 1:
                    return function(data, args[1], **kwargs)
                return function(data, **kwargs)
            except ValidationError as e:
                error_dict = e.args[0]
                error_para = next(iter(error_dict.keys()))
                error_para_msg = error_dict[error_para][0]
                if error_para_msg == 'Missing data for required field.':
                    return send_error_response(missing_param_name=error_para,
                                               status_code=HTTPStatus.BAD_REQUEST)
                else:
                    return send_error_response(invalid_param_name=error_para,
                                               status_code=HTTPStatus.BAD_REQUEST)
        return wrapper
    return decorate_func
