from flask import current_app as app
from flask import jsonify


@app.errorhandler(406)
def resource_not_found(err):
    return jsonify(message=str(err)), 406
