from my_project.user.models import User, TokenBlocklist
from my_project import database_conn
from my_project import bcrypt
from my_project.user.schema import UserRegisterResponseSchema
from my_project import constant
from flask_jwt_extended import create_access_token, create_refresh_token
from http import HTTPStatus
from sqlalchemy.exc import SQLAlchemyError
from flask import current_app as app


def register_user(**kwargs):
    """
    This method takes all required parameters and register user.
    :param kwargs: dict of user data.
    :return: tuple of Response components, For success or Error
    """
    try:
        # Todo: Check if we can get exact user instead of getting list and then first of it
        user_name = kwargs.get("user_name")
        password = kwargs.get("password")
        email = kwargs.get("email")

        if not User.query.filter(User.email == email).first():
            if not User.query.filter(User.user_name == user_name).first():
                hash_password = bcrypt.generate_password_hash(password).decode('utf-8')
                kwargs["password"] = hash_password
                user = User(kwargs)
                user.save()
                data = UserRegisterResponseSchema().dump(user)
                success_msg = constant.MSG_REGISTER_USER_SUCCESSFULLY.format(user_name)
                return HTTPStatus.CREATED, success_msg, data
            else:
                err_msg = constant.ERR_USER_NAME_ALREADY_TAKEN.format(user_name)
                return HTTPStatus.BAD_REQUEST, err_msg, None
        else:
            err_msg = constant.ERR_EMAIL_ALREADY_TAKEN.format(email)
            return HTTPStatus.BAD_REQUEST, err_msg, None
    except SQLAlchemyError as e:
        app.logger.error(e.__str__())
        return HTTPStatus.INTERNAL_SERVER_ERROR, constant.ERR_SQL_ALCHEMY_ERROR, None


def login_user(**data):
    """
    This method is used to login the user
    :param data: data for a user login
    :return: tuple of Response components, For success or Error
    """
    user_name = data.get("user_name")
    password = data.get("password")
    user = User.query.filter(User.user_name == user_name).first()
    if not user:
        return HTTPStatus.FORBIDDEN, constant.ERR_USER_WITH_USER_NAME_NOT_EXISTS.format(user_name), None

    if not bcrypt.check_password_hash(user.password, password):
        return HTTPStatus.FORBIDDEN, constant.ERR_PASSWORD_INCORRECT, None

    access_token = create_access_token(identity=user.user_id)
    refresh_token = create_refresh_token(identity=user.user_id)
    data = UserRegisterResponseSchema().dump(user)
    data["access_token"] = access_token
    data["refresh_token"] = refresh_token
    return HTTPStatus.OK, constant.MSG_LOG_IN_SUCCESSFULLY, data


def create_token_blocklist(jti, ttype, now):
    """
    This could be expanded to fit the needs of your application. For example,
    it could track who revoked a JWT, when a token expires, notes for why a
    JWT was revoked, an endpoint to un-revoked a JWT, etc.
    Making jti an index can significantly speed up the search when there are
    tens of thousands of records. Remember this query will happen for every
    (protected) request,
    If your database supports a UUID type, this can be used for the jti column
    as well
    """
    database_conn.session.add(TokenBlocklist(jti=jti, type=ttype, created_at=now))
    database_conn.session.commit()
    return HTTPStatus.NO_CONTENT, constant.MSG_LOG_OUT_SUCCESSFULLY, None

