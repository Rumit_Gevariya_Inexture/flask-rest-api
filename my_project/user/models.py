from datetime import datetime
from my_project import database_conn
from my_project import jwt
from flask_jwt_extended import get_current_user


class User(database_conn.Model):
    # To change actual table name in the database, As by default it will be same as the model name
    __tablename__ = 'users'

    user_id = database_conn.Column(database_conn.INTEGER, primary_key=True)
    user_name = database_conn.Column(database_conn.String(30), nullable=False, unique=True)
    first_name = database_conn.Column(database_conn.String(50), nullable=False)
    last_name = database_conn.Column(database_conn.String(50), nullable=False)
    password = database_conn.Column(database_conn.String(200), nullable=False)
    email = database_conn.Column(database_conn.String(62), nullable=False, unique=True)
    created_at = database_conn.Column(database_conn.DateTime, default=datetime.now())

    def __repr__(self):
        return f"< User {self.user_id}>"

    def __init__(self, data):
        self.user_name = data.get("user_name")
        self.first_name = data.get("first_name")
        self.last_name = data.get("last_name")
        self.email = data.get("email")
        self.password = data.get("password")

    def save(self):
        database_conn.session.add(self)
        database_conn.session.commit()


# Register a callback function that takes whatever object is passed in as the
# identity when creating JWTs and converts it to a JSON serializable format.
@jwt.user_identity_loader
def user_identity_lookup(self):
    return self


@jwt.user_lookup_loader
def user_lookup_callback(_jwt_header, jwt_data):
    """
        Register a callback function that loads a user from your database whenever
        a protected route is accessed. This should return any python object on a
        successful lookup, or None if the lookup failed for any reason (for example
        if the user has been deleted from the database).
    """
    identity = jwt_data["sub"]
    return User.query.filter_by(user_id=identity).one_or_none()


class TokenBlocklist(database_conn.Model):
    id = database_conn.Column(database_conn.Integer, primary_key=True)
    jti = database_conn.Column(database_conn.String(100), nullable=False, index=True)
    type = database_conn.Column(database_conn.String(16), nullable=False)
    user_id = database_conn.Column(database_conn.ForeignKey('users.user_id', ondelete="CASCADE"),
                                   default=lambda: get_current_user().user_id, nullable=False,)
    created_at = database_conn.Column(database_conn.DateTime, nullable=False)


@jwt.token_in_blocklist_loader
def check_if_token_revoked(jwt_header, jwt_payload: dict) -> bool:
    """
    callback function to check if a JT exists in the database blocklist.
    :param jwt_header 
    :param jwt_payload 
    :return: 
    """
    jti = jwt_payload["jti"]
    token = database_conn.session.query(TokenBlocklist.id).filter_by(jti=jti).scalar()
    return token is not None
