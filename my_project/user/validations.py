import re
from marshmallow.exceptions import ValidationError
from my_project import constant


def username_validation(user_name):
    regex = constant.USER_NAME_REGEX

    if re.fullmatch(regex, user_name):
        return user_name
    else:
        raise ValidationError("Please enter valid username!")


def email_validation(email):
    regex = constant.EMAIL_REGEX

    if re.fullmatch(regex, email):
        return email
    else:
        raise ValidationError("Please enter valid email!")


def password_validation(password):
    regex = constant.PASSWORD_REGEX

    if re.fullmatch(regex, password):
        return password
    else:
        raise ValidationError("Please enter valid password!")
