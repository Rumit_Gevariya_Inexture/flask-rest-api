from flask import Blueprint, request, jsonify
from flask_restful import Api, Resource
from flask_accept import accept
from flask_jwt_extended import jwt_required
from flask_jwt_extended import get_jwt
from flask_jwt_extended import get_jwt_identity, create_access_token
from my_project.user import request_response_handler
from datetime import datetime
from datetime import timezone

# Blueprint Configuration
auth_bp = Blueprint('auth_bp', __name__)
auth_rest_api = Api(auth_bp)


@auth_bp.route("/test_server")
def server_check():
    """
    A Simple function to check if server is working state or not 
    :return: str: Simple message
    """
    return "Server is working"

# Router Inside a Blueprint


class UserRegistation(Resource):

    @accept("application/json")
    def post(self):
        """
        This is call when request method is post.
        :return:
        """
        req_data = request.get_json()
        return request_response_handler.create_response_of_register_user(req_data)


class UserLogin(Resource):

    @accept("application/json")
    def post(self):
        """
        This is called when request method is post.
        :return:
        """
        req_data = request.get_json()
        return request_response_handler.create_response_of_login_user(req_data)


class UserLogout(Resource):
    @jwt_required(refresh=True)
    def delete(self):
        """
        This is called when request method is delete.
        Endpoint for revoking the current users refresh token. Saved the unique
        identifier (jti) for the JWT into our database.
        :return:
        """
        token = get_jwt()
        jti = token["jti"]
        ttype = token["type"]
        now = datetime.now(timezone.utc)
        return request_response_handler.create_response_of_logout_user(jti, ttype, now)


@auth_bp.post('/token/refresh')
@jwt_required(refresh=True)
def refresh_user_token():
    """
    This is called when request method is get.
    use to generate the access token from the refresh token.
    :return:
    """
    identity = get_jwt_identity()
    access_token = create_access_token(identity=identity)
    return request_response_handler.create_response_of_refresh_user_token(access_token)


auth_rest_api.add_resource(UserRegistation, '/api/v1/auth/register/',)
auth_rest_api.add_resource(UserLogin, '/api/v1/auth/login/',)
auth_rest_api.add_resource(UserLogout, '/api/v1/auth/logout/',)
