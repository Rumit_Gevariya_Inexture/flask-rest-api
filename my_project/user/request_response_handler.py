# create responses of user module's api here
"""
This file checks the request is proper or not.
and this must also creates the Response object, all other business logic can be there in the other file.
"""
from http import HTTPStatus

from my_project import constant
from my_project import response_utils
from my_project.request_utils import validate_req_schema
from my_project.user.schema import UserRegisterRequestSchema, UserLoginRequestSchema
from my_project.user import services


@validate_req_schema(UserRegisterRequestSchema())
def create_response_of_register_user(request_data):
    """
    From this request get the required params and if all available then proceed
    otherwise send missing params response
    :param request_data: Data from API request
    :return: Json formed response
    """

    http_status_code, message, data = services.register_user(**request_data)
    if http_status_code == HTTPStatus.CREATED:
        return response_utils.send_success_response(message=message, data=data, status_code=http_status_code)

    return response_utils.send_error_response(status_code=http_status_code, message=message)


@validate_req_schema(UserLoginRequestSchema())
def create_response_of_login_user(request_data):
    print(request_data)
    """
   From this request get the required params and if all available then proceed
   otherwise send missing params response
   :param request_data: Data from API request
   :return: Json formed response
   """
    http_status_code, message, data = services.login_user(**request_data)
    if http_status_code == HTTPStatus.OK:
        return response_utils.send_success_response(message=message, data=data, status_code=http_status_code)
    return response_utils.send_error_response(status_code=http_status_code, message=message)


def create_response_of_refresh_user_token(access_token):
    return response_utils.send_success_response(message=constant.MSG_FOR_ACCESS_TOKEN_REFRESH,
                                                data=access_token, status_code=HTTPStatus.CREATED)


def create_response_of_logout_user(jti, ttype, now):
    http_status_code, message, data = services.create_token_blocklist(jti, ttype, now)
    if http_status_code == HTTPStatus.NO_CONTENT:
        return response_utils.send_success_response(message=message, data=data, status_code=http_status_code)

    return response_utils.send_error_response(status_code=http_status_code, message=message)
