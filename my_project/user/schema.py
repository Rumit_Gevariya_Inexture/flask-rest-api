from my_project import marshmallow
from flask_marshmallow.fields import fields
from my_project.user.models import User
from my_project.user.validations import (
                                        username_validation,
                                        email_validation,
                                        password_validation
                                        )
from marshmallow import validate


class UserRegisterRequestSchema(marshmallow.Schema):
    user_name = fields.Str(required=True, validate=[validate.Length(min=6, max=12),username_validation],)
    first_name = fields.Str(required=True, validate=[validate.Length(min=1, max=50)])
    last_name = fields.Str(required=True, validate=[validate.Length(min=1, max=50)])
    password = fields.Str(required=True, validate=password_validation)
    email = fields.Email(required=True, validate=email_validation)


class UserRegisterResponseSchema(marshmallow.SQLAlchemyAutoSchema):
    class Meta:
        model = User
        exclude = ('password', 'created_at')


class UserLoginRequestSchema(marshmallow.Schema):
    user_name = fields.Str(required=True, validate=[validate.Length(min=6, max=12), username_validation])
    password = fields.Str(required=True, validate=password_validation)