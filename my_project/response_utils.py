from http import HTTPStatus


def send_success_response(message, data=None, status_code=HTTPStatus.OK):
    """
   Return success response with status RESPONSE_STATUS_SUCCESS
   :param status_code: HTTP status code
   :param data: data to be sent in response
   :param message: message to pass
   :return: success response
   """
    response = {'message': message or 'Success'}
    if data:
        response["data"] = data
    return response, status_code


def send_error_response(status_code, message=None, missing_param_name =None, invalid_param_name =None):
    """
    Return error response with status RESPONSE_STATUS_ERROR
    :param status_code: HTTP status code
    :param invalid_param_name: If provided then create message with @get_param_invalid_msg here
    :param missing_param_name: If provided then create message with @get_param_missing_msg here
    :param message: message to pass
    :return: error response
    """
    response = {}

    if message:
        response['message'] = message
    elif missing_param_name:
        response['message'] = __get_param_missing_msg(missing_param_name)
    elif invalid_param_name:
        response['message'] = __get_param_invalid_msg(invalid_param_name)
    else:
        response["message"] = status_code.description
    return response, status_code


def __get_param_missing_msg(parameter_name):
    """
     Use this fun to return error message whenever any parameter is missing from API
     :param parameter_name = Name of missing parameter in request
     :return: str: message
    """

    return f"{parameter_name} is required."


def __get_param_invalid_msg(parameter_name):
    """
    Use this fun to return error message whenever any parameter is invalid from API
    :param parameter_name: Name of parameter
    :return: str: message
    """
    return f"{parameter_name} is not valid."

