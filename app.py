"""Application entry point."""

from my_project import create_app

app = create_app()

if __name__ == "__main__":
    app.run(host="0.0.0.0")  # default port is 5000, you can change port and host
